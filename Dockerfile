FROM python:latest
RUN MKDIR /app
RUN chmod 755 /app
WORKDIR /app
COPY ./app.py .
EXPOSE 8080
USER 1001
ENTRYPOINT["python","app.py"]

