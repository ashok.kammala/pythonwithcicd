#!/bin/bash
if [ -z ${TRIVY_VERSION} ]; then
export TRIVY_VERSION=${TRIVY_VERSION:-0.46.0}
echo $TRIVY_VERSION
wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v0.46.0/trivy_0.46.0
_Linux-64bit.tar.gz -O - | tar -zxvf -
echo "TRIVY INSTALLED"
./trivy image --format json --output result.json ${IMAGE_TAG}
./trivy image --exit-code 1 --severity CRITICAL ${IMAGE_TAG}
else
echo "TRIVY IS ALREADY INSTALLED"
./trivy image --format json --output result.json ${IMAGE_TAG}
./trivy image --exit-code 1 --severity CRITICAL ${IMAGE_TAG}
fi